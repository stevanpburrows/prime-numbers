package com.examples.my.primenumbers;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by stevanburrows on 06/12/2017.
 */
@RunWith(AndroidJUnit4.class)
public class PrimeNumbersActivityTest {

    ActivityTestRule<PrimeNumbersActivity> mActivityTestRule = new ActivityTestRule<>(PrimeNumbersActivity.class);

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testFirstNumberEditTextIsDisplayed() throws Exception {

        mActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.first_number_text)).check(matches(isDisplayed()));
    }

    @Test
    public void testErrorMessageDisplayedIfFirstNumberInvalid() throws Exception {

        mActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.update_button)).check(matches(isDisplayed())).perform(click());
        onView(withText("First number is not a number")).inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void testErrorMessageDisplayedIfSecondNumberInvalid() throws Exception {

        mActivityTestRule.launchActivity(new Intent());
        onView(withId(R.id.first_number_text)).perform(typeText("2"));
        onView(withId(R.id.update_button)).check(matches(isDisplayed())).perform(click());
        onView(withText("Second number is not a number")).inRoot(withDecorView(not(is(mActivityTestRule.getActivity().getWindow().getDecorView())))).check(matches(isDisplayed()));
    }

    @Test
    public void testNumberListDisplayed() throws Exception {

        // given
        String primeListText = " \n" + "2" + " \n" + "3" + " \n" + "5" + " \n" + "7";

        // when
        mActivityTestRule.launchActivity(new Intent());

        // then
        onView(withId(R.id.first_number_text)).perform(typeText("1"));
        onView(withId(R.id.second_number_text)).perform(typeText("10"));
        onView(withId(R.id.update_button)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.output_text)).check(matches(isDisplayed()));
        onView(withText(primeListText)).check(matches(isDisplayed()));
    }

    @Test
    public void testNotPrimeNumberListDisplayed() throws Exception {

        // given
        String primeListText = " \n" + "1" + " \n" + "4" + " \n" + "6" + " \n" + "8" + " \n" + "9"
                + " \n" + "10";

        // when
        mActivityTestRule.launchActivity(new Intent());

        // then
        onView(withId(R.id.first_number_text)).perform(typeText("1"));
        onView(withId(R.id.second_number_text)).perform(typeText("10"));
        onView(withId(R.id.prime_switch)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.update_button)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.output_text)).check(matches(isDisplayed()));
        onView(withText(primeListText)).check(matches(isDisplayed()));
    }
}