package com.examples.my.primenumbers;

import java.util.ArrayList;

/**
 * Created by stevanburrows on 06/12/2017.
 */

public class Prime {

    public static boolean isPrime(int number) {

        if (number == 1)
            return false;

        for (int i = 2; i < number; i++) {
            if (number % i == 0)
                return false;
        }

        return true;
    }

    public static ArrayList<Integer> getPrimeList(int firstNumber, int lastNumber) {

        // Switch number values if first is more than last
        if (firstNumber > lastNumber) {
            int tempNumber = firstNumber;
            firstNumber = lastNumber;
            lastNumber = tempNumber;
        }

        ArrayList<Integer> primeList = new ArrayList<>();

        for (int i = firstNumber; i <= lastNumber; i++) {
            if (isPrime(i))
                primeList.add(i);
        }

        return primeList;
    }

    public static ArrayList<Integer> getNotPrimeList(int firstNumber, int lastNumber) {

        // Switch number values if first is more than last
        if (firstNumber > lastNumber) {
            int tempNumber = firstNumber;
            firstNumber = lastNumber;
            lastNumber = tempNumber;
        }

        ArrayList<Integer> notPrimeList = new ArrayList<>();

        for (int i = firstNumber; i <= lastNumber; i++) {
            if (!isPrime(i))
                notPrimeList.add(i);
        }

        return notPrimeList;
    }
}
