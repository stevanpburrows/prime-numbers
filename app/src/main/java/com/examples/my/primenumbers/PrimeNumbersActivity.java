package com.examples.my.primenumbers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PrimeNumbersActivity extends AppCompatActivity implements PrimeNumbersView {

    private PrimeNumbersPresenter mPresenter;

    private EditText mFirstNumber, mSecondNumber;
    private Switch mPrimeSwitch;
    private Button mUpdateButton;
    private TextView mOutputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prime_numbers);

        initialiseViews();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new PrimeNumbersPresenter(this);
    }

    private void initialiseViews() {

        mFirstNumber = (EditText) findViewById(R.id.first_number_text);
        mSecondNumber = (EditText) findViewById(R.id.second_number_text);
        mPrimeSwitch = (Switch) findViewById(R.id.prime_switch);
        mUpdateButton = (Button) findViewById(R.id.update_button);
        mOutputText = (TextView) findViewById(R.id.output_text);

        mUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.updatePrimeList(
                        mFirstNumber.getText().toString(),
                        mSecondNumber.getText().toString(),
                        mPrimeSwitch.isChecked());
            }
        });
    }

    @Override
    public void updateNumberList(ArrayList<Integer> numberList) {

        mOutputText.setText("");

        for (Integer number : numberList) {
            mOutputText.setText(mOutputText.getText().toString() + " \n" + number);
        }
    }

    @Override
    public void firstNumberInvalid() {

        Toast.makeText(this, "First number is not a number", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void secondNumberInvalid() {

        Toast.makeText(this, "Second number is not a number", Toast.LENGTH_SHORT).show();
    }
}
