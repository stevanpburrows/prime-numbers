package com.examples.my.primenumbers;

import android.widget.Switch;

/**
 * Created by stevanburrows on 06/12/2017.
 */

public class PrimeNumbersPresenter {

    private PrimeNumbersView mContract;

    public PrimeNumbersPresenter(PrimeNumbersView contract) {
        this.mContract = contract;
    }

    public void updatePrimeList(String firstNumber, String lastNumber, boolean isPrime) {

        if (!isValidNumber(firstNumber)) {
            mContract.firstNumberInvalid();
            return;
        }

        if (!isValidNumber(lastNumber)) {
            mContract.secondNumberInvalid();
            return;
        }

        int first = Integer.valueOf(firstNumber);
        int last = Integer.valueOf(lastNumber);

        if (isPrime) {
            mContract.updateNumberList(Prime.getPrimeList(first, last));
        } else {
            mContract.updateNumberList(Prime.getNotPrimeList(first, last));
        }
    }

    public boolean isValidNumber (String numberString) {

        if(numberString.matches("\\d+(?:\\.\\d+)?")) {
            return true;
        }

        return false;
    }
}
