package com.examples.my.primenumbers;

import java.util.ArrayList;

/**
 * Created by stevanburrows on 06/12/2017.
 */

public interface PrimeNumbersView {

    void updateNumberList(ArrayList<Integer> numberList);

    void firstNumberInvalid();

    void secondNumberInvalid();
}
