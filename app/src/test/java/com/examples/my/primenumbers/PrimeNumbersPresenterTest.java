package com.examples.my.primenumbers;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by stevanburrows on 06/12/2017.
 */
public class PrimeNumbersPresenterTest {

    @Test
    public void testIsValidNumber() throws Exception {

        PrimeNumbersView view = mock(PrimeNumbersView.class);
        PrimeNumbersPresenter presenter = new PrimeNumbersPresenter(view);
        assertTrue(presenter.isValidNumber("2"));
    }

    @Test
    public void testUpdatePrimeList() throws Exception {

        PrimeNumbersView view = mock(PrimeNumbersView.class);
        PrimeNumbersPresenter presenter = new PrimeNumbersPresenter(view);

        presenter.updatePrimeList("1", "100", true);
        ArrayList<Integer> mockList = new ArrayList<>();
        for (Integer number : Prime.getPrimeList(1, 100)) {
            mockList.add(number);
        }
        verify(view).updateNumberList(mockList);
    }

    @Test
    public void testUpdateNotPrimeList() throws Exception {

        PrimeNumbersView view = mock(PrimeNumbersView.class);
        PrimeNumbersPresenter presenter = new PrimeNumbersPresenter(view);

        presenter.updatePrimeList("1", "100", false);
        ArrayList<Integer> mockList = new ArrayList<>();
        for (Integer number : Prime.getNotPrimeList(1, 100)) {
            mockList.add(number);
        }
        verify(view).updateNumberList(mockList);
    }

    @Test
    public void testUpdatePrimeListInvalidFirstNumber() throws Exception {

        PrimeNumbersView view = mock(PrimeNumbersView.class);
        PrimeNumbersPresenter presenter = new PrimeNumbersPresenter(view);

        presenter.updatePrimeList("", "", true);
        verify(view).firstNumberInvalid();
    }

    @Test
    public void testUpdatePrimeListInvalidSecondNumber() throws Exception {

        PrimeNumbersView view = mock(PrimeNumbersView.class);
        PrimeNumbersPresenter presenter = new PrimeNumbersPresenter(view);

        presenter.updatePrimeList("1", "", true);
        verify(view).secondNumberInvalid();
    }
}