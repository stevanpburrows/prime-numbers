package com.examples.my.primenumbers;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by stevanburrows on 06/12/2017.
 */
public class PrimeTest {

    @Test
    public void testIsPrime() throws Exception {
        assertTrue(Prime.isPrime(11));
    }

    @Test
    public void testOneNotPrime() throws Exception {
        assertFalse(Prime.isPrime(1));
    }

    @Test
    public void testNotPrime() throws Exception {
        assertFalse(Prime.isPrime(20));
    }

    @Test
    public void testPrimesExist() throws Exception {
        assertFalse(Prime.getPrimeList(1, 100).isEmpty());
    }

    @Test
    public void testNoPrimesExist() throws Exception {
        assertTrue(Prime.getPrimeList(1, 1).isEmpty());
    }

    @Test
    public void testFirstPrimeIsTwo() throws Exception {
        assertEquals(Prime.getPrimeList(100,1).get(0).intValue(), 2);
    }

    @Test
    public void testTenthPrimeIsTwentyNine() throws Exception {
        assertEquals(Prime.getPrimeList(1,100).get(9).intValue(), 29);
    }

    @Test
    public void testPrimeFirstNumberMoreThanLast() throws Exception {
        assertEquals(Prime.getPrimeList(100,1).get(0).intValue(), 2);
    }

    @Test
    public void testNotPrimesExist() throws Exception {
        assertFalse(Prime.getNotPrimeList(1, 100).isEmpty());
    }

    @Test
    public void testNoNotPrimesExist() throws Exception {
        assertTrue(Prime.getNotPrimeList(2, 3).isEmpty());
    }

    @Test
    public void testFirstNotPrimeIsOne() throws Exception {
        assertEquals(Prime.getNotPrimeList(100,1).get(0).intValue(), 1);
    }

    @Test
    public void testTenthNotPrimeIsTwentyNine() throws Exception {
        assertEquals(Prime.getNotPrimeList(1,100).get(9).intValue(), 10);
    }

    @Test
    public void testNotPrimeFirstNumberMoreThanLast() throws Exception {
        assertEquals(Prime.getNotPrimeList(100,1).get(0).intValue(), 1);
    }
}